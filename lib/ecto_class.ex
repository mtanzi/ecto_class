defmodule EctoClass do
  @moduledoc """
  Documentation for EctoClass.
  """

  @doc """
  Hello world.

  ## Examples

      iex> EctoClass.hello
      :world

  """
  def hello do
    :world
  end
end
